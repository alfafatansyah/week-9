/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#define no_0 0x003F	// 0b 0000 0000 0011 1111
#define no_1 0x0406	// 0b 0000 0100 0000 0110
#define no_2 0x00DB	// 0b 0000 0000 1101 1011
#define no_3 0x008F	// 0b 0000 0000 1000 1111
#define no_4 0x00E6	// 0b 0000 0000 1110 0110
#define no_5 0x00ED	// 0b 0000 0000 1110 1101
#define no_6 0x00FD	// 0b 0000 0000 1111 1101
#define no_7 0x1401	// 0b 0001 0100 0000 0001
#define no_8 0x00FF	// 0b 0000 0000 1111 1111
#define no_9 0x00E7	// 0b 0000 0000 1110 0111
#define n_T 0x1201	// 0b 0001 0010 0000 0001
#define n_d 0x0482	// 0b 0000 0100 1000 0010
#define n_C 0x0039	// 0b 0000 0000 0011 1001
#define n_H 0x00F6	// 0b 0000 0000 1111 0110
#define n_L 0x0038	// 0b 0000 0000 0011 1000
#define n_ 0x00C0		// 0b 0000 0000 1100 0000
#define n_E 0x00F9	// 0b 0000 0000 1111 1001
#define n_R 0x08F3		// 0b 0000 1000 1111 0011

#define sym_bt 0x0080	// row 0
#define sym_r 0x0040	// row 0
#define sym_fm 0x0080	// row 1
#define sym_bazz 0x0040	// row 1
#define sym_card 0x0080	// row 2
#define sym_mhz 0x0040	// row 2
#define sym_usb 0x0080	// row 3
#define sym_dotf 0x0040	// row 3
#define sym_wifi 0x0080	// row 4
#define sym_fotb 0x0040	// row 4
#define sym_music 0x0080	// row 5
#define sym_dot 0x0040 	// row 5
#define sym_video 0x0080	// row 6
#define sym_pic 0x0080	// row 7
#define sym_play 0x0080	// row 8
#define sym_pause 0x0080 // row 9
#define sym_repeat 0x0080	// row 10
#define sym_rand 0x0080	// row 11
#define sym_mic 0x0080	// row 12
#define sym_l 0x0080	// row 13

#define cs_low HAL_GPIO_WritePin(STB_GPIO_Port, STB_Pin, 0)
#define cs_high HAL_GPIO_WritePin(STB_GPIO_Port, STB_Pin, 1)
#define wr_low HAL_GPIO_WritePin(CLK_GPIO_Port, CLK_Pin, 0)
#define wr_high HAL_GPIO_WritePin(CLK_GPIO_Port, CLK_Pin, 1)
#define data_low HAL_GPIO_WritePin(DIO_GPIO_Port, DIO_Pin, 0)
#define data_high HAL_GPIO_WritePin(DIO_GPIO_Port, DIO_Pin, 1)
#define cmp_low HAL_GPIO_WritePin(CMP_GPIO_Port, CMP_Pin, 0)
#define cmp_high HAL_GPIO_WritePin(CMP_GPIO_Port, CMP_Pin, 1)
	
#define id_read 0x06	// read
#define id_write 0x05	// write
#define id_cmd 0x04		// command

#define code_sys_dis 0x00
#define code_sys_en 0x01
#define code_led_off 0x02
#define code_led_on 0x03
#define code_blynk_off 0x08
#define code_blynk_on 0x09
#define code_slave_mode 0x10
#define code_master_mode 0x18
#define code_ext_clk_mode 0x1C
#define code_com_nmos_8 0x20
#define code_com_nmos_16 0x24
#define code_com_pmos_8 0x28
#define code_com_pmos_16 0x2C
#define code_pwm_1_16 0xA0
#define code_pwm_2_16 0xA1
#define code_pwm_3_16 0xA2
#define code_pwm_4_16 0xA3
#define code_pwm_5_16 0xA4
#define code_pwm_6_16 0xA5
#define code_pwm_7_16 0xA6
#define code_pwm_8_16 0xA7
#define code_pwm_9_16 0xA8
#define code_pwm_10_16 0xA9
#define code_pwm_11_16 0xAA
#define code_pwm_12_16 0xAB
#define code_pwm_13_16 0xAC
#define code_pwm_14_16 0xAD
#define code_pwm_15_16 0xAE
#define code_pwm_16_16 0xAF

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED_INTERNAL_Pin GPIO_PIN_13
#define LED_INTERNAL_GPIO_Port GPIOC
#define STB_Pin GPIO_PIN_0
#define STB_GPIO_Port GPIOA
#define CLK_Pin GPIO_PIN_1
#define CLK_GPIO_Port GPIOA
#define DIO_Pin GPIO_PIN_2
#define DIO_GPIO_Port GPIOA
#define PCLK_Pin GPIO_PIN_3
#define PCLK_GPIO_Port GPIOA
#define PDT_Pin GPIO_PIN_4
#define PDT_GPIO_Port GPIOA
#define IRM_Pin GPIO_PIN_7
#define IRM_GPIO_Port GPIOA
#define IRM_EXTI_IRQn EXTI9_5_IRQn
#define CMP_Pin GPIO_PIN_15
#define CMP_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
