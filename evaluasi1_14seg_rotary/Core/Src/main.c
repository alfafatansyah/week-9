/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim10;
TIM_HandleTypeDef htim11;

/* USER CODE BEGIN PV */
uint32_t tempcode, code, last_code, count_on, count_off, timeout, temp_millis, temp_last_millis, error_millis;
//uint16_t current_state_CLK, last_state_CLK;
uint16_t digit_seg[8];
uint8_t bitindex, cmd, cmdli, mode, next_mode, temperature, temperature_last, cut_on, cut_off, set_H, set_L, set_input, status_input;
uint8_t segment[14];
_Bool	power_status, display_status, status_display, warning, status_cmp, sensor_error, change, CLK, DT, current_state_CLK, last_state_CLK;
_Bool error_av[7];
int test, test2, e, temp;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM10_Init(void);
static void MX_TIM11_Init(void);
/* USER CODE BEGIN PFP */
void write_data (uint8_t id_data, uint8_t cmd_data, uint16_t data_data);
void ht_1632c_init (void);
void print_segment(uint16_t header, uint32_t number);

uint16_t convert_character(uint16_t chara);
uint8_t convert_code (uint32_t code);
uint16_t map(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min, uint16_t out_max);
void read_button(uint8_t state);
void display_mode (void);
void compressor (void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();
  MX_TIM10_Init();
  MX_TIM11_Init();
  /* USER CODE BEGIN 2 */
	HAL_TIM_Base_Start(&htim1);
  __HAL_TIM_SET_COUNTER(&htim1, 0);
	HAL_TIM_Base_Start_IT(&htim10);
	HAL_TIM_Base_Start_IT(&htim11);
	
	ht_1632c_init();
	set_H = 5;	// default cut on
	set_L = 1;	// default cut off
	cut_on = set_H;
	cut_off = set_L;
	next_mode = 3;
	last_state_CLK = HAL_GPIO_ReadPin(PCLK_GPIO_Port, PCLK_Pin);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		//HAL_GPIO_TogglePin(LED_INTERNAL_GPIO_Port, LED_INTERNAL_Pin);
		
		if(mode != 0)
		{
			if(warning != 0)
			{
				for(int i = 0; i < 8; i++)
				{
					status_display = !status_display;	
					if(warning == 0)
					{
						status_display = 0;
						break;
					}
					HAL_Delay(75);
				}
				
				if(next_mode == 0)
					mode = 0;
				else if(next_mode == 1)
					mode = 1;
				else if(next_mode == 2)
					mode = 2;
				next_mode = 3;				
				status_display = 0;
				warning = 0;
			}
		}	
			
		HAL_Delay(1);	
		
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 100;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 100-1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 0xffff-1;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM10 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM10_Init(void)
{

  /* USER CODE BEGIN TIM10_Init 0 */

  /* USER CODE END TIM10_Init 0 */

  /* USER CODE BEGIN TIM10_Init 1 */

  /* USER CODE END TIM10_Init 1 */
  htim10.Instance = TIM10;
  htim10.Init.Prescaler = 100-1;
  htim10.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim10.Init.Period = 0xffff-1;
  htim10.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim10.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim10) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM10_Init 2 */

  /* USER CODE END TIM10_Init 2 */

}

/**
  * @brief TIM11 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM11_Init(void)
{

  /* USER CODE BEGIN TIM11_Init 0 */

  /* USER CODE END TIM11_Init 0 */

  /* USER CODE BEGIN TIM11_Init 1 */

  /* USER CODE END TIM11_Init 1 */
  htim11.Instance = TIM11;
  htim11.Init.Prescaler = 10000-1;
  htim11.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim11.Init.Period = 10000-1;
  htim11.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim11.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim11) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM11_Init 2 */

  /* USER CODE END TIM11_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_INTERNAL_GPIO_Port, LED_INTERNAL_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, STB_Pin|CLK_Pin|DIO_Pin|CMP_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : LED_INTERNAL_Pin */
  GPIO_InitStruct.Pin = LED_INTERNAL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_INTERNAL_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : STB_Pin CLK_Pin DIO_Pin CMP_Pin */
  GPIO_InitStruct.Pin = STB_Pin|CLK_Pin|DIO_Pin|CMP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PCLK_Pin PDT_Pin */
  GPIO_InitStruct.Pin = PCLK_Pin|PDT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : IRM_Pin */
  GPIO_InitStruct.Pin = IRM_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(IRM_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */
void write_data (uint8_t id_data, uint8_t cmd_data, uint16_t data_data)
{
	uint8_t temp;
	cs_low;	
	for(int i = 1 << 2; i > 0; i >>= 1)
	{
		temp = (id_data & i) ? 1 : 0;
		wr_low;
		HAL_GPIO_WritePin(DIO_GPIO_Port, DIO_Pin, temp);
		wr_high;
	}	
	switch(id_data)
	{
		case id_cmd:
			for(int i = 1 << 7; i > 0; i >>=1)
			{
				temp = (cmd_data & i) ? 1 : 0;
				wr_low;
				HAL_GPIO_WritePin(DIO_GPIO_Port, DIO_Pin, temp);
				wr_high;
			}
			wr_low;
			HAL_GPIO_WritePin(DIO_GPIO_Port, DIO_Pin, temp);
			wr_high;
			break;
		case id_write:
			for(int i = 1 << 6; i > 0; i >>=1)
			{
				temp = (cmd_data & i) ? 1 : 0;
				wr_low;
				HAL_GPIO_WritePin(DIO_GPIO_Port, DIO_Pin, temp);
				wr_high;
			}
			for(int i = 1 << 7; i > 0; i >>=1)
			{
				temp = (data_data & i) ? 1 : 0;
				wr_low;
				HAL_GPIO_WritePin(DIO_GPIO_Port, DIO_Pin, temp);
				wr_high;
			}
			break;
	}
	cs_high;
}

void ht_1632c_init (void)
{
	write_data(id_cmd, code_sys_dis, 0);
	write_data(id_cmd, code_com_nmos_16, 0);
	write_data(id_cmd, code_master_mode, 0);
	write_data(id_cmd, code_sys_en, 0);
	write_data(id_cmd, code_led_on, 0);
	for(int i = 0; i <= 0x5E; i+=2)
	{
		write_data(id_write, i, 0x00);
	}
}

void print_segment(uint16_t header, uint32_t number)
{	
	int p;
	if(display_status == 1)
	{
		if(sensor_error == 0)
		{
			digit_seg[3] = header;
			if(status_display == 0)
			{
				if(number >= 10)
					digit_seg[4] = convert_character(number % 100 /10);
				else
					digit_seg[4] = 0x0000;
				digit_seg[5] = convert_character(number % 10);
			}
			else
			{
				digit_seg[4] = 0x0000;
				digit_seg[5] = 0x0000;
			}
			digit_seg[6] = n_d;
			digit_seg[7] = n_C;		
			write_data(id_write, 0x0E, sym_dot);
		}
		else if(sensor_error == 1)
		{
			digit_seg[3] = n_E;
			digit_seg[4] = n_R;
			digit_seg[5] = n_R;
			digit_seg[6] = no_0;
			digit_seg[7] = n_R;
			write_data(id_write, 0x0E, 0x0000);
		}		
		p = 1;
		for(int i = 0; i < 14; i++)
		{
			for(int j = 0; j < 8; j++)
			{
				segment[i] <<= 1;
				segment[i] = (digit_seg[j] & p) ? segment[i] | 1 : segment[i] | 0;
			}
			p = p << 1;
		}
	}
	else
	{
		for(int i = 0; i < 14; i++)
		{
			segment[i] = 0x0000;
		}
		write_data(id_write, 0x0E, 0x0000);
	}
		
	p = 0;
	for(int i = 0x00; i <= 0x34; i+=4)
	{
		write_data(id_write, i, segment[p]);
		p++;
	}
}

uint16_t convert_character(uint16_t chara)
{
	switch (chara)
	{
		case 0:
        return no_0;
        break;
    case 1:
        return no_1;
        break;
    case 2:
        return no_2;
        break;
    case 3:
        return no_3;
        break;
    case 4:
        return no_4;
        break;
    case 5:
        return no_5;
        break;
    case 6:
        return no_6;
        break;
    case 7:
        return no_7;
        break;
    case 8:
        return no_8;
        break;
    case 9:
        return no_9;
        break;
   }
}

uint8_t convert_code (uint32_t code)
{
	switch (code)
	{
		case (0x8976E817):	// Power button
			return 21;
			break;		
		case (0x897650AF):	// Source button
			return 22;
			break;		
		case (0x89761AE5):	// Bluetooth button
			return 23;
			break;		
		case (0x8976807F):	// 1 button
			return 1;
			break;
		case (0x897640BF):	// 2 button
			return 2;
			break;
		case (0x8976C03F):	// 3 button
			return 3;
			break;
		case (0x897620DF):	// 4 button
			return 4;
			break;
		case (0x8976A05F):	// 5 button
			return 5;
			break;
		case (0x8976609F):	// 6 button
			return 6;
			break;
		case (0x8976E01F):	// 7 button
			return 7;
			break;
		case (0x897610EF):	// 8 button
			return 8;
			break;			
		case (0x8976906F):	// 9 button
			return 9;
			break;					
		case (0x897600FF):	// 0 button
			return 0;
			break;			
		case (0x8976C837):	// Play / Pause button
			return 11;
			break;		
		case (0x89768A75):	// Vol - button
			return 12;
			break;			
		case (0x89760AF5):	// Vol + button
			return 13;
			break;		
		case (0x8976D827):	// Prev button
			return 14;
			break;		
		case (0x897658A7):	// Next button
			return 15;
			break;
		case (0x897616E9):	// Scan button
			return 16;
			break;		
		case (0x89768877):	// Memory button
			return 17;
			break;		
		case (0x89763CC3):	// Light button
			return 18;
			break;		
		case (0x8976A857):	// Mute button
			return 19;
			break;		
		case (0x89767887):	// Repeat button
			return 24;
			break;		
		case (0x89764AB5):	// Karaoke button
			return 25;
			break;		
		case (0x89765CA3):	// Bass button
			return 26;
			break;		
		case (0x8976BC43):	// Middle button
			return 27;
			break;		
		case (0x89761CE3):	// Treble button
			return 88;
			break;		
		case (0x8976BA45):	// Super Bass button
			return 29;
			break;		
		case (0x89768C73):	// Eq button
			return 30;
			break;		
		case (0x89760CF3):	// Bazzoke button
			return 31;
			break;		
		default:
      return 99;
	}
}

uint16_t map(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min, uint16_t out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void read_button(uint8_t state)
{
	if(power_status == 0)	// OFF condition
	{
		if(state == 21)	// power ON
		{
			status_cmp = 0;
			count_off = 0;
			display_status = 1;
			power_status = 1;
		}
	}
	else	// ON condition
	{
		if(state == 21)	// power OFF
		{			
			status_cmp = 1;
			count_on = 10;
			display_status = 0;
			mode = 0;
			power_status = 0;
		}
		if(state == 17)	// change mode HIGH
		{			
			if(mode != 1)
			{
				if(mode == 0)
					mode = 1;
				else if(mode == 2)
				{
					if(status_input != 0)
					{
						set_L = set_input;
						mode = 1;
					}
					else
						mode = 1;
				}
				timeout = 0;
			}
			else 
			{
				if(status_input != 0)
				{
					if(set_input <= set_L)
					{
						next_mode = 0;
						warning = 1;	
					}
					else
					{
						set_H = set_input;
						mode = 0;
					}
				}
				else
					mode = 0;					
			}
			status_input = 0;
			set_input = 0;	
		}
		if(state == 16)	// change mode LOW
		{			
			if(mode != 2)
			{
				if(mode == 0)
					mode = 2;
				else if(mode == 1)
				{
					if(status_input != 0)
					{
						if(set_input <= set_L)
						{
							next_mode = 2;	
							warning = 1;						
						}
						else
						{
							set_H = set_input;	
							mode = 2;
						}
					}
					else
						mode = 2;
				}				
				timeout = 0;
			}
			else 
			{
				if(status_input != 0)
				{
					if(set_input >= set_H)
					{
						next_mode = 0;
						warning = 1;
					}
					else
					{
						set_L = set_input;
						mode = 0;
					}
				}
				else
					mode = 0;
			}
			status_input = 0;
			set_input = 0;
		}
		if(state == 13) // value++
		{
			timeout = 0;
			if(mode == 1)	// set cut on
			{
				if(set_H >= 99)
					warning = 1;
				else
					set_H++;
			}
			else if(mode == 2)	// set cut off
			{
				if(set_L >= set_H - 1)
					warning = 1;
				else
					set_L++;
			}
		}
	
		if(state == 12)	// value--
		{
			timeout = 0;
			if(mode == 1)	// set cut on
			{
				if(set_H <= set_L + 1)
					warning = 1;
				else
					set_H--;
			}
			else if(mode == 2)	// set cut off
			{
				if(set_L <= 0)
					warning = 1;
				else
					set_L--;
			}
		}	
		if(state == 1 || state == 2 || state == 3 || state == 4 || state == 5 || state == 6 || state == 7 || state == 8 || state == 9 || state == 0)
		{
			if(mode != 0)
			{
				timeout = 0;
				status_input++;
				if(status_input == 1)
					set_input = set_input + state;
					if(mode == 1 && set_L >= 10 && set_input < set_L / 10 || mode == 1 && set_L < 10 && set_input <= set_L)
					{
						warning = 1;
						status_input = 0;
						set_input = 0;
					}
					else if(mode == 2 && set_input >= set_H)
					{
						warning = 1;
						status_input = 0;
						set_input = 0;
					}	
				else if(status_input == 2)
				{
					set_input = set_input * 10 + state;
					if(mode == 1)
					{
						if(set_input < set_L + 1)
							warning = 1;
						else
							set_H = set_input;
					}
					else if(mode == 2)
					{
						if(set_input > set_H - 1)
							warning = 1;
						else
							set_L = set_input;
					}
					status_input = 0;
					set_input = 0;
				}
			}
		}
	}
}

void display_mode (void)
{
	if(mode == 0)	// default mode
	{
		cut_on = set_H;
		cut_off = set_L;
		set_input = 0;
		status_input = 0;		
		print_segment(n_T, temperature);
	}
	else if(mode == 1)	// cut on mode
	{
		if(status_input == 0)
			print_segment(n_H, set_H);
		else
			print_segment(n_H, set_input);
	}
	else if(mode == 2)	// cut off mode
	{
		if(status_input == 0)
			print_segment(n_L, set_L);
		else
			print_segment(n_L, set_input);
	}
}

void compressor (void)
{
	if(temperature >= cut_on && count_off >= 5 && power_status == 1)
	{
		cmp_high;
		HAL_GPIO_WritePin(LED_INTERNAL_GPIO_Port, LED_INTERNAL_Pin, 0);
		status_cmp = 1;
	}
	else if(temperature <= cut_off && count_on >= 10|| power_status == 0)
	{
		cmp_low;
		HAL_GPIO_WritePin(LED_INTERNAL_GPIO_Port, LED_INTERNAL_Pin, 1);
		status_cmp = 0;
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if (htim == &htim10)
	{
		CLK = HAL_GPIO_ReadPin(PCLK_GPIO_Port, PCLK_Pin);
		DT = HAL_GPIO_ReadPin(PDT_GPIO_Port, PDT_Pin);
		
		current_state_CLK = HAL_GPIO_ReadPin(PCLK_GPIO_Port, PCLK_Pin);
		
		if(current_state_CLK != last_state_CLK && current_state_CLK == 1)
		{
			if(HAL_GPIO_ReadPin(PDT_GPIO_Port, PDT_Pin) != current_state_CLK)
			{
				temperature++;
				if(temperature > 99)
					temperature = 99;
			}
			else
			{
				temperature--;
				if(temperature > 99)
					temperature = 0;
			}
		}
		
		last_state_CLK = current_state_CLK;
		
		read_button(convert_code(code));
		
		if (code != 0)
		{
			last_code = code;		
			code = 0;
		}
		
		display_mode();	
		compressor();
	}	
	if (htim == &htim11)
	{
		if(status_cmp == 1)
		{
			count_on++;
			count_off = 0;
		}
		else
		{
			count_on = 0;
			count_off++;
		}		
		
		if(mode != 0)
		{
			timeout++;
			if(timeout >= 10)
			{
				if(mode == 1)
				{
					if(status_input != 0)
					{
						if(set_input <= set_L)
						{
							warning = 1;	
							next_mode = 0;
						}
						else
						{
							set_H = set_input;
							mode = 0;
						}
					}
					else
						mode = 0;
				}
				else if(mode == 2)
				{
					if(status_input != 0)
					{
						if(set_input >= set_H)
						{
							warning = 1;
							next_mode = 0;
						}
						else
						{
							set_L = set_input;
							mode = 0;
						}
					}
					else
						mode = 0;
				}
			}
		}
		else
			timeout = 0;
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == IRM_Pin)
	{
		if(__HAL_TIM_GET_COUNTER(&htim1) > 8000)
		{
			tempcode = 0;
			bitindex = 0;
		}
		else if(__HAL_TIM_GET_COUNTER(&htim1) > 1700)
		{
			tempcode |= (1UL << (31-bitindex));	// write 1
			bitindex++;
		}
		else if(__HAL_TIM_GET_COUNTER(&htim1) > 1000)
		{
			tempcode &= ~(1UL << (31-bitindex));	// write 0
			bitindex++;
		}		
		if(bitindex == 32)
		{
			cmdli = ~tempcode;	// logical inverted last 8 bits
			cmd = tempcode >> 8;	// second last 8 bits
			if(cmdli == cmd)	// check for errors
			{
				code = tempcode;	// if no bit errors				
			}
			bitindex = 0;
		}
			__HAL_TIM_SET_COUNTER(&htim1, 0);
	}	
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
