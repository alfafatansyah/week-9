/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim16;
TIM_HandleTypeDef htim17;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
uint32_t tempcode, code, last_code, count_on, count_off, timeout, temp_millis, temp_last_millis, error_millis;
uint16_t AD_RES = 0;
uint8_t bitindex, cmd, cmdli, mode, next_mode, temperature, temperature_last, cut_on, cut_off, set_H, set_L, set_input, status_input;
uint8_t data_cmd_1, data_cmd_2, data_cmd_3, data_cmd_4, bitindex, cmdli, cmd;
uint8_t dig_sel[14] = {dig_1, dig_2, dig_3, dig_4, dig_5, dig_6, dig_7, dig_1_ext, dig_2_ext, dig_3_ext, dig_4_ext, dig_5_ext, dig_6_ext, dig_7_ext};
uint8_t pw_sel[8] = {pw_1_16, pw_2_16, pw_4_16, pw_10_16, pw_11_16, pw_12_16, pw_13_16, pw_14_16};
_Bool	power_status, display_status, status_display, warning, status_cmp, sensor_error, change;
_Bool error_av[7];
int test, test2, e, temp;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM16_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM17_Init(void);
/* USER CODE BEGIN PFP */
void delay_us (uint16_t us);
void set_cmd_1 (uint8_t cmd_1_display);
void set_cmd_2 (uint8_t cmd_2_mode, uint8_t cmd_2_add, uint8_t cmd_2_wr);
void set_cmd_3 (uint8_t cmd_3_digit, uint8_t data);
void set_cmd_4 (uint8_t cmd_4_pulse, uint8_t cmd_4_display);
void reception (uint8_t cmd_set, uint8_t cmd_data); // Reception (Data / Command Write)
void initial_setting(void);
void print_segment(uint8_t header, uint32_t print_data);
uint8_t convert_character(uint8_t chara);
uint8_t convert_code (uint32_t code);
uint16_t map(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min, uint16_t out_max);
void read_button(uint8_t state);
void display_mode (void);
void compressor (void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM16_Init();
  MX_ADC1_Init();
  MX_TIM3_Init();
  MX_TIM17_Init();
  /* USER CODE BEGIN 2 */
	HAL_TIM_Base_Start(&htim1);
  __HAL_TIM_SET_COUNTER(&htim1, 0);
	HAL_TIM_Base_Start(&htim2);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
  // Calibrate The ADC On Power-Up For Better Accuracy
  HAL_ADCEx_Calibration_Start(&hadc1);
	HAL_TIM_Base_Start_IT(&htim16);
	HAL_TIM_Base_Start_IT(&htim17);

	initial_setting();
	set_H = 5;	// default cut on
	set_L = 1;	// default cut off
	cut_on = set_H;
	cut_off = set_L;
	next_mode = 3;	
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		//HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_6);
		
		if(mode != 0)
		{
			if(warning != 0)
			{
				for(int i = 0; i < 8; i++)
				{
					status_display = !status_display;	
					if(warning == 0)
					{
						status_display = 0;
						break;
					}
					HAL_Delay(75);
				}
				
				if(next_mode == 0)
					mode = 0;
				else if(next_mode == 1)
					mode = 1;
				else if(next_mode == 2)
					mode = 2;
				next_mode = 3;				
				status_display = 0;
				warning = 0;
			}
		}	
			
		HAL_Delay(1);		
    
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the peripherals clocks
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC|RCC_PERIPHCLK_TIM1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_SYSCLK;
  PeriphClkInit.Tim1ClockSelection = RCC_TIM1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.LowPowerAutoPowerOff = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.SamplingTimeCommon1 = ADC_SAMPLETIME_1CYCLE_5;
  hadc1.Init.SamplingTimeCommon2 = ADC_SAMPLETIME_1CYCLE_5;
  hadc1.Init.OversamplingMode = DISABLE;
  hadc1.Init.TriggerFrequencyMode = ADC_TRIGGER_FREQ_HIGH;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_7;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 64-1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 0xffff-1;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 64-1;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 0xffff-1;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 65535;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief TIM16 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM16_Init(void)
{

  /* USER CODE BEGIN TIM16_Init 0 */

  /* USER CODE END TIM16_Init 0 */

  /* USER CODE BEGIN TIM16_Init 1 */

  /* USER CODE END TIM16_Init 1 */
  htim16.Instance = TIM16;
  htim16.Init.Prescaler = 64-1;
  htim16.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim16.Init.Period = 0xffff-1;
  htim16.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim16.Init.RepetitionCounter = 0;
  htim16.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim16) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM16_Init 2 */

  /* USER CODE END TIM16_Init 2 */

}

/**
  * @brief TIM17 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM17_Init(void)
{

  /* USER CODE BEGIN TIM17_Init 0 */

  /* USER CODE END TIM17_Init 0 */

  /* USER CODE BEGIN TIM17_Init 1 */

  /* USER CODE END TIM17_Init 1 */
  htim17.Instance = TIM17;
  htim17.Init.Prescaler = 6400-1;
  htim17.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim17.Init.Period = 10000-1;
  htim17.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim17.Init.RepetitionCounter = 0;
  htim17.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim17) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM17_Init 2 */

  /* USER CODE END TIM17_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_7B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, STB_Pin|CLK_Pin|DIO_Pin|CMP_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : T_NRST_Pin */
  GPIO_InitStruct.Pin = T_NRST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(T_NRST_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : STB_Pin CLK_Pin DIO_Pin CMP_Pin */
  GPIO_InitStruct.Pin = STB_Pin|CLK_Pin|DIO_Pin|CMP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : IRM_Pin */
  GPIO_InitStruct.Pin = IRM_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(IRM_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD3_Pin */
  GPIO_InitStruct.Pin = LD3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD3_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);

}

/* USER CODE BEGIN 4 */
void delay_us (uint16_t us)
{
	__HAL_TIM_SET_COUNTER(&htim2,0);  // set the counter value a 0
	while (__HAL_TIM_GET_COUNTER(&htim2) < us);  // wait for the counter to reach the us input in the parameter
}

void set_cmd_1 (uint8_t cmd_1_display)
{
	data_cmd_1 = header_cmd_1 | cmd_1_display;
	reception(data_cmd_1, 0);
}

void set_cmd_2 (uint8_t cmd_2_mode, uint8_t cmd_2_add, uint8_t cmd_2_wr)
{
	data_cmd_2 = header_cmd_2 | cmd_2_mode | cmd_2_add | cmd_2_wr;
	reception(data_cmd_2, 0);
}

void set_cmd_3 (uint8_t cmd_3_digit, uint8_t data)
{
	data_cmd_3 = header_cmd_3 | cmd_3_digit;
	reception(data_cmd_3, data);
}

void set_cmd_4 (uint8_t cmd_4_pulse, uint8_t cmd_4_display)
{
	uint8_t temp;
	if(cmd_4_display == 0)
		temp = display_off;
	else
		temp = display_on;
	
	data_cmd_4 = header_cmd_4 | cmd_4_pulse | temp;
	reception(data_cmd_4, 0);
}

void reception (uint8_t cmd_set, uint8_t cmd_data) // Reception (Data / Command Write)
{	
	uint8_t temp;
	stb_low;	
	delay_us(1);
	
	for(int i = 1; i <= 1 << 7; i = i << 1)
	{
		temp = (cmd_set & i) ? 1 : 0;
		clk_low;
		HAL_GPIO_WritePin(DIO_GPIO_Port, DIO_Pin, temp);
		delay_us(1);
		clk_high;
		delay_us(1);
	}
	
	if(cmd_set >= header_cmd_3)
	{
		delay_us(1);
		for(int i = 1; i <= 1 << 7; i = i << 1)
		{
			temp = (cmd_data & i) ? 1 : 0;
			clk_low;
			HAL_GPIO_WritePin(DIO_GPIO_Port, DIO_Pin, temp);
			delay_us(1);
			clk_high;
			delay_us(1);
		}
	}
	stb_high;
	delay_us(1);
}

void initial_setting(void)
{
	HAL_Delay(200);
	set_cmd_2(data_write, 0, 0);
	for(int i = 0; i < 14; i++)
	{
		set_cmd_3(dig_sel[i], 0);
	}		
	set_cmd_1(d7_s10);
	for(int i = 0; i < 8; i++)
	{
		set_cmd_4(pw_sel[i], 1);
	}		
	set_cmd_1(d7_s10);
	for(int i = 0; i < 8; i++)
	{
		set_cmd_4(pw_sel[i], 0);
	}		
}

void print_segment(uint8_t header, uint32_t print_data)
{
	uint8_t s1 = print_data % 100 / 10;
	uint8_t s0 = print_data % 10;
	
	set_cmd_2(data_write, inc_add, normal_mode);
	
	if(sensor_error == 0)
	{
		set_cmd_3(dig_1, header);
		if(status_display == 0)
		{
			if(print_data >= 10)
				set_cmd_3(dig_2, convert_character(s1) | dot_2_b);
			else
				set_cmd_3(dig_2, 0x80);
			set_cmd_3(dig_3, convert_character(s0));
		}
		else
		{		
			set_cmd_3(dig_2, 0x80);
			set_cmd_3(dig_3, 0x00);
		}
		set_cmd_3(dig_4, n_d);	
		set_cmd_3(dig_5, n_C);
	}
	else
	{
		set_cmd_3(dig_1, n_e);
		set_cmd_3(dig_2, n_r);
		set_cmd_3(dig_3, n_r);
		set_cmd_3(dig_4, n_0);	
		set_cmd_3(dig_5, n_r);
	}
	
	set_cmd_1(d7_s10);
	set_cmd_4(pw_14_16, display_status);
}

uint8_t convert_character(uint8_t chara)
{
	switch (chara)
	{
		case 0:
        return n_0;
        break;
    case 1:
        return n_1;
        break;
    case 2:
        return n_2;
        break;
    case 3:
        return n_3;
        break;
    case 4:
        return n_4;
        break;
    case 5:
        return n_5;
        break;
    case 6:
        return n_6;
        break;
    case 7:
        return n_7;
        break;
    case 8:
        return n_8;
        break;
    case 9:
        return n_9;
        break;
   }
}

uint8_t convert_code (uint32_t code)
{
	switch (code)
	{
		case (0x8976E817):	// Power button
			return 21;
			break;		
		case (0x897650AF):	// Source button
			return 22;
			break;		
		case (0x89761AE5):	// Bluetooth button
			return 23;
			break;		
		case (0x8976807F):	// 1 button
			return 1;
			break;
		case (0x897640BF):	// 2 button
			return 2;
			break;
		case (0x8976C03F):	// 3 button
			return 3;
			break;
		case (0x897620DF):	// 4 button
			return 4;
			break;
		case (0x8976A05F):	// 5 button
			return 5;
			break;
		case (0x8976609F):	// 6 button
			return 6;
			break;
		case (0x8976E01F):	// 7 button
			return 7;
			break;
		case (0x897610EF):	// 8 button
			return 8;
			break;			
		case (0x8976906F):	// 9 button
			return 9;
			break;					
		case (0x897600FF):	// 0 button
			return 0;
			break;			
		case (0x8976C837):	// Play / Pause button
			return 11;
			break;		
		case (0x89768A75):	// Vol - button
			return 12;
			break;			
		case (0x89760AF5):	// Vol + button
			return 13;
			break;		
		case (0x8976D827):	// Prev button
			return 14;
			break;		
		case (0x897658A7):	// Next button
			return 15;
			break;
		case (0x897616E9):	// Scan button
			return 16;
			break;		
		case (0x89768877):	// Memory button
			return 17;
			break;		
		case (0x89763CC3):	// Light button
			return 18;
			break;		
		case (0x8976A857):	// Mute button
			return 19;
			break;		
		case (0x89767887):	// Repeat button
			return 24;
			break;		
		case (0x89764AB5):	// Karaoke button
			return 25;
			break;		
		case (0x89765CA3):	// Bass button
			return 26;
			break;		
		case (0x8976BC43):	// Middle button
			return 27;
			break;		
		case (0x89761CE3):	// Treble button
			return 88;
			break;		
		case (0x8976BA45):	// Super Bass button
			return 29;
			break;		
		case (0x89768C73):	// Eq button
			return 30;
			break;		
		case (0x89760CF3):	// Bazzoke button
			return 31;
			break;		
		default:
      return 99;
	}
}

uint16_t map(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min, uint16_t out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void read_button(uint8_t state)
{
	if(power_status == 0)	// OFF condition
	{
		if(state == 21)	// power ON
		{
			status_cmp = 0;
			count_off = 0;
			display_status = 1;
			power_status = 1;
		}
	}
	else	// ON condition
	{
		if(state == 21)	// power OFF
		{			
			status_cmp = 1;
			count_on = 10;
			display_status = 0;
			mode = 0;
			power_status = 0;
		}
		if(state == 17)	// change mode HIGH
		{			
			if(mode != 1)
			{
				if(mode == 0)
					mode = 1;
				else if(mode == 2)
				{
					if(status_input != 0)
					{
						set_L = set_input;
						mode = 1;
					}
					else
						mode = 1;
				}
				timeout = 0;
			}
			else 
			{
				if(status_input != 0)
				{
					if(set_input <= set_L)
					{
						next_mode = 0;
						warning = 1;	
					}
					else
					{
						set_H = set_input;
						mode = 0;
					}
				}
				else
					mode = 0;					
			}
			status_input = 0;
			set_input = 0;	
		}
		if(state == 16)	// change mode LOW
		{			
			if(mode != 2)
			{
				if(mode == 0)
					mode = 2;
				else if(mode == 1)
				{
					if(status_input != 0)
					{
						if(set_input <= set_L)
						{
							next_mode = 2;	
							warning = 1;						
						}
						else
						{
							set_H = set_input;	
							mode = 2;
						}
					}
					else
						mode = 2;
				}				
				timeout = 0;
			}
			else 
			{
				if(status_input != 0)
				{
					if(set_input >= set_H)
					{
						next_mode = 0;
						warning = 1;
					}
					else
					{
						set_L = set_input;
						mode = 0;
					}
				}
				else
					mode = 0;
			}
			status_input = 0;
			set_input = 0;
		}
		if(state == 13) // value++
		{
			timeout = 0;
			if(mode == 1)	// set cut on
			{
				if(set_H >= 99)
					warning = 1;
				else
					set_H++;
			}
			else if(mode == 2)	// set cut off
			{
				if(set_L >= set_H - 1)
					warning = 1;
				else
					set_L++;
			}
		}
	
		if(state == 12)	// value--
		{
			timeout = 0;
			if(mode == 1)	// set cut on
			{
				if(set_H <= set_L + 1)
					warning = 1;
				else
					set_H--;
			}
			else if(mode == 2)	// set cut off
			{
				if(set_L <= 0)
					warning = 1;
				else
					set_L--;
			}
		}	
		if(state == 1 || state == 2 || state == 3 || state == 4 || state == 5 || state == 6 || state == 7 || state == 8 || state == 9 || state == 0)
		{
			if(mode != 0)
			{
				timeout = 0;
				status_input++;
				if(status_input == 1)
					set_input = set_input + state;
					if(mode == 2 && set_input >= set_H)
					{
						warning = 1;
						status_input = 0;
						set_input = 0;
					}	
				else if(status_input == 2)
				{
					set_input = set_input * 10 + state;
					if(mode == 1)
					{
						if(set_input <= set_L + 1)
							warning = 1;
						else
							set_H = set_input;
					}
					else if(mode == 2)
					{
						if(set_input >= set_H - 1)
							warning = 1;
						else
							set_L = set_input;
					}
					status_input = 0;
					set_input = 0;
				}
			}
		}
	}
}

void display_mode (void)
{
	if(mode == 0)	// default mode
	{
		cut_on = set_H;
		cut_off = set_L;
		set_input = 0;
		status_input = 0;
		HAL_ADC_Start_IT(&hadc1);
		print_segment(n_t, temperature);
	}
	else if(mode == 1)	// cut on mode
	{
		if(status_input == 0)
			print_segment(n_H, set_H);
		else
			print_segment(n_H, set_input);
	}
	else if(mode == 2)	// cut off mode
	{
		if(status_input == 0)
			print_segment(n_L, set_L);
		else
			print_segment(n_L, set_input);
	}
}

void compressor (void)
{
	if(temperature >= cut_on && count_off >= 5 && sensor_error == 0)
	{
		cmp_high;
		status_cmp = 1;
	}
	else if(temperature <= cut_off && count_on >= 10)
	{
		cmp_low;
		status_cmp = 0;
	}
	else if(count_on >= 10 && sensor_error == 1)
	{
		cmp_low;
		status_cmp = 0;
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if (htim == &htim16)
	{
		read_button(convert_code(code));
		
		if (code != 0)
		{
			last_code = code;		
			code = 0;
		}
		
		display_mode();	
		compressor();
	}
	
	if (htim == &htim17)
	{
		if(status_cmp == 1)
		{
			count_on++;
			count_off = 0;
		}
		else
		{
			count_on = 0;
			count_off++;
		}		
		
		if(mode != 0)
		{
			timeout++;
			if(timeout >= 10)
			{
				if(mode == 1)
				{
					if(status_input != 0)
					{
						if(set_input <= set_L)
						{
							warning = 1;	
							next_mode = 0;
						}
						else
						{
							set_H = set_input;
							mode = 0;
						}
					}
					else
						mode = 0;
				}
				else if(mode == 2)
				{
					if(status_input != 0)
					{
						if(set_input >= set_H)
						{
							warning = 1;
							next_mode = 0;
						}
						else
						{
							set_L = set_input;
							mode = 0;
						}
					}
					else
						mode = 0;
				}
			}
		}
		else
			timeout = 0;
	}
}

void HAL_GPIO_EXTI_Falling_Callback(uint16_t GPIO_Pin)
{
	test = __HAL_TIM_GET_COUNTER(&htim1);
	if(GPIO_Pin == IRM_Pin)
	{
		if(__HAL_TIM_GET_COUNTER(&htim1) > 8000)
		{
			tempcode = 0;
			bitindex = 0;
		}
		else if(__HAL_TIM_GET_COUNTER(&htim1) > 1700)
		{
			tempcode |= (1UL << (31-bitindex));	// write 1
			bitindex++;
		}
		else if(__HAL_TIM_GET_COUNTER(&htim1) > 1000)
		{
			tempcode &= ~(1UL << (31-bitindex));	// write 0
			bitindex++;
		}		
		if(bitindex == 32)
		{
			cmdli = ~tempcode;	// logical inverted last 8 bits
			cmd = tempcode >> 8;	// second last 8 bits
			if(cmdli == cmd)	// check for errors
			{
				code = tempcode;	// if no bit errors				
			}
			bitindex = 0;
		}
			__HAL_TIM_SET_COUNTER(&htim1, 0);
	}
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	// Read & Update The ADC Result
  AD_RES = HAL_ADC_GetValue(&hadc1);
	if(power_status == 1)
		temperature = map(AD_RES, 0 , 4095, 0, 99);
	else
		temperature = 0;
	
	temp_millis++;
	test = temp_millis - temp_last_millis;
	test2 = abs(temperature - temperature_last);
	
	if(abs(temperature - temperature_last) > 8 && temp_millis - temp_last_millis < 6)
	{
		//sensor_error = 1;
		error_av[e] = 1;
	}
	else
	{
		//sensor_error = 0;
		error_av[e] = 0;
	}
	temperature_last = temperature;
	temp_last_millis = temp_millis;
	
	//error_av[e] = sensor_error;
	e++;
	if(e > 7)
		e = 0;
	
	temp = 0;
	for(int i = 0; i < 7; i++)
		temp = temp + error_av[i];
	
	if(temp < 3)
		sensor_error = 0;
	else
		sensor_error = 1;
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
