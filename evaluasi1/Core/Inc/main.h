/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#define n_0 0x3f			//0b00111111
#define n_1 0x06			// 0x00000110
#define n_2 0x5B			// 0b01011011
#define n_3 0x4F			// 0b01001111
#define n_4 0x66			// 0b01100110
#define n_5 0x6D			// 0b01101101
#define n_6 0x7D			// 0b01111101
#define n_7 0x07			// 0b00000111
#define n_8 0x7F			// 0b01111111
#define n_9 0x6F			// 0b01101111
#define n_t 0x78			// 0b01111000
#define n_d 0x63			// 0b01100011
#define n_C 0x39			// 0b00111001
#define n_H 0x76			// 0b01110110
#define n_L 0x38			// 0b00111000
#define n_ 0x40			// 0b01000000
#define n_e 0x7B			// 0b01111011
#define n_r 0x31			// 0b00110001

#define v_1 0x80			// 0b10000000	(digit 1)
#define v_2 0x01			// 0b00000001	(digit 1 ext)
#define dot_2_f 0x80	// 0b10000000	(digit 2)
#define dot_2_b 0x80	// 0b10000000	(digit 3)
#define dot_1 0x01 		// 0b00000001	(digit 4 ext)
#define mic_1 0x01 		// 0b00000001	(digit 6) 
#define mic_2 0x02 		// 0b00000010	(digit 6) 
#define l 0x04 				// 0b00000100	(digit 6) 
#define music 0x08 		// 0b00001000	(digit 6) 
#define bazz 0x10 		// 0b00010000	(digit 6) 
#define video 0x20 		// 0b00100000	(digit 6) 
#define mhz 0x40 			// 0b01000000	(digit 6) 
#define picture 0x80 	// 0b10000000	(digit 6) 
#define r 0x01 				// 0b00000001	(digit 6 ext) 
#define rec 0x01 			// 0b00000001	(digit 7) 
#define sound 0x02 		// 0b00000010	(digit 7) 
#define rand 0x04 		// 0b00000100	(digit 7) 
#define loop 0x08 		// 0b00001000	(digit 7) 
#define play 0x10 		// 0b00010000	(digit 7) 
#define pause 0x20 		// 0b00100000	(digit 7) 
#define usb 0x40 			// 0b01000000	(digit 7) 
#define card 0x01 		// 0b00000001	(digit 7 ext) 

#define stb_low HAL_GPIO_WritePin(STB_GPIO_Port, STB_Pin, 0)
#define stb_high HAL_GPIO_WritePin(STB_GPIO_Port, STB_Pin, 1)
#define dio_low HAL_GPIO_WritePin(DIO_GPIO_Port, DIO_Pin, 0)
#define dio_high HAL_GPIO_WritePin(DIO_GPIO_Port, DIO_Pin, 1)
#define clk_low HAL_GPIO_WritePin(CLK_GPIO_Port, CLK_Pin, 0)
#define clk_high HAL_GPIO_WritePin(CLK_GPIO_Port, CLK_Pin, 1)
#define cmp_low HAL_GPIO_WritePin(CMP_GPIO_Port, CMP_Pin, 0)
#define cmp_high HAL_GPIO_WritePin(CMP_GPIO_Port, CMP_Pin, 1)

#define header_cmd_1 0x00	// COMMANDS 1: DISPLAY MODE SETTING COMMANDS
#define d4_s13 0x00				// Displa mode settings: 4 digits, 13 segments
#define d5_s12 0x01				// Displa mode settings: 5 digits, 12 segments
#define d6_s11 0x02				// Displa mode settings: 6 digits, 11 segments
#define d7_s10 0x03				// Displa mode settings: 7 digits, 10 segments

#define header_cmd_2 0x40	// COMMANDS 2: DATA SETTING COMMANDS
#define data_write 0x00		// Write data to display mode
#define data_read 0x02		// Read key data
#define inc_add 0x00			// Increment address after data has been written
#define fix_add 0x04			// Fixed address
#define normal_mode 0x00	// Normal operation mode
#define test_mode 0x08		// Test Mode

#define header_cmd_3 0xC0	// COMMANDS 3: ADDRESS SETTING COMMANDS
#define dig_1 0x00				// DIG 1, SG1-SG4 / SG5-SG8
#define dig_2 0x02				// DIG 2, SG1-SG4 / SG5-SG8
#define dig_3 0x04				// DIG 3, SG1-SG4 / SG5-SG8
#define dig_4 0x06				// DIG 4, SG1-SG4 / SG5-SG8
#define dig_5 0x08				// DIG 5, SG1-SG4 / SG5-SG8
#define dig_6 0x0A				// DIG 6, SG1-SG4 / SG5-SG8
#define dig_7 0x0C				// DIG 7, SG1-SG4 / SG5-SG8
#define dig_1_ext 0x01		// DIG 1, SG9-SG10 / SG13-SG14
#define dig_2_ext 0x03		// DIG 2, SG9-SG10 / SG13-SG14
#define dig_3_ext 0x05		// DIG 3, SG9-SG10 / SG13-SG14
#define dig_4_ext 0x07		// DIG 4, SG9-SG10 / SG13-SG14
#define dig_5_ext 0x09		// DIG 5, SG9-SG10 / SG13-SG14
#define dig_6_ext 0x0B		// DIG 6, SG9-SG10 / SG13-SG14
#define dig_7_ext 0x0D		// DIG 7, SG9-SG10 / SG13-SG14

#define header_cmd_4 0x80 // COMMANDS 4: DISPLAY CONTROL COMMANDS
#define pw_1_16 0x00 			// Pulse width=1/16
#define pw_2_16 0x01 			// Pulse width=2/16
#define pw_4_16 0x02 			// Pulse width=4/16
#define pw_10_16 0x03			// Pulse width=10/16
#define pw_11_16 0x04 		// Pulse width=11/16
#define pw_12_16 0x05			// Pulse width=12/16
#define pw_13_16 0x06			// Pulse width=13/16
#define pw_14_16 0x07			// Pulse width=14/16
#define display_off 0x00	// Display off (Key scan continues)
#define display_on 0x08		// Display on

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define T_NRST_Pin GPIO_PIN_2
#define T_NRST_GPIO_Port GPIOF
#define STB_Pin GPIO_PIN_0
#define STB_GPIO_Port GPIOA
#define CLK_Pin GPIO_PIN_1
#define CLK_GPIO_Port GPIOA
#define T_VCP_TX_Pin GPIO_PIN_2
#define T_VCP_TX_GPIO_Port GPIOA
#define T_VCP_RX_Pin GPIO_PIN_3
#define T_VCP_RX_GPIO_Port GPIOA
#define DIO_Pin GPIO_PIN_4
#define DIO_GPIO_Port GPIOA
#define IRM_Pin GPIO_PIN_5
#define IRM_GPIO_Port GPIOA
#define IRM_EXTI_IRQn EXTI4_15_IRQn
#define CMP_Pin GPIO_PIN_6
#define CMP_GPIO_Port GPIOA
#define LD3_Pin GPIO_PIN_6
#define LD3_GPIO_Port GPIOC
#define T_JTMS_Pin GPIO_PIN_13
#define T_JTMS_GPIO_Port GPIOA
#define T_JTCK_Pin GPIO_PIN_14
#define T_JTCK_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
