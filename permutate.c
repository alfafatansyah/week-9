#include <stdio.h>
#include <string.h>

void permute(char *str, int l, int r)
{
    if (l == r)
        printf("%s\n", str);
    else
    {
        for (int i = l; i <= r; i++)
        {
            // Swap characters at index l and i
            char temp = str[l];
            str[l] = str[i];
            str[i] = temp;

            // Recurse on the sub-string str[l+1, r]
            permute(str, l+1, r);

            // Restore the original string
            temp = str[l];
            str[l] = str[i];
            str[i] = temp;
        }
    }
}

int main()
{
    char str[] = "1234";
    int n = strlen(str);
    permute(str, 0, n-1);
    return 0;
}
